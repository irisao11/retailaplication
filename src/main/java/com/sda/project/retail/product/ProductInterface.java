package com.sda.project.retail.product;

import com.sda.project.retail.client.Client;

import java.util.List;

public interface ProductInterface {

    public void insertProduct(Product product);

    public Product getProduct(int id);

    public List<Product> getAllProducts();

    public void deleteProduct(int id);

    public void deleteAllProducts();

    public void updateProduct(Product product);
}
