package com.sda.project.retail.product;

public class Product {
    private int productId;
    private int price;
    private String colour;
    private String type;
    private String barCode;

    public Product() {
    }

    public Product(int productId, int price, String colour, String type, String barCode) {
        this.productId = productId;
        this.price = price;
        this.colour = colour;
        this.type = type;
        this.barCode = barCode;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", price=" + price +
                ", colour='" + colour + '\'' +
                ", type='" + type + '\'' +
                ", barCode='" + barCode + '\'' +
                '}';
    }
}
