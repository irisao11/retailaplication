package com.sda.project.retail.product;

import com.sda.project.retail.client.ClientDAO;
import com.sda.project.retail.utils.GenericCRUD;

import java.util.ArrayList;
import java.util.List;

//clasa tip singleton
public class ProductDAO extends GenericCRUD<Product> {

    private List<Product> products;
    //variabila tip clasa pt ca folosim singleton
    private static ProductDAO instance;

    //constructor privat pt ca avem singleton
    private ProductDAO() {
        products = new ArrayList<>();
    }

    //metoda getInstance pt singleton
    public static ProductDAO getInstance() {
        if (instance == null) {
            instance = new ProductDAO();
        }
        return instance;
    }

    public ProductDAO(List<Product> products) {
        this.products = products;
    }

    @Override
    public void insert(Product element) {
        this.products.add(element);
    }

    @Override
    public Product get(int id) {
        for (Product product : products) {
            if (id == product.getProductId()) {
                return product;
            }
        }
        return null;
    }

    @Override
    public List<Product> getAll() {
        return products;
    }

    @Override
    public void delete(int id) {
        for (Product product : products) {
            if (id == product.getProductId()) {
                products.remove(product);
                break;
            }
        }

    }

    @Override
    public void deleteAll() {
        products.clear();
    }

    @Override
    public void update(Product element) {
        for (Product product : products) {
            if (element.getProductId() == product.getProductId()) {
                product.setType(element.getType());
                product.setBarCode(element.getBarCode());
                product.setColour(element.getColour());
                product.setPrice(element.getPrice());
            }
        }
    }
}
