package com.sda.project.retail.product;

import com.sda.project.retail.client.Client;
import com.sda.project.retail.client.ClientDAO;
import com.sda.project.retail.client.ClientReader;
import com.sda.project.retail.utils.Consts;
//atentie de unde se import Loggerul, tb sa fie cu "apache"
import org.apache.log4j.Logger;

import java.util.List;

public class ProductService implements ProductInterface {

    final static Logger logger = Logger.getLogger(ProductService.class);
    private ProductDAO productDAO;
    private ProductReader productReader;

    public ProductService() {
        productDAO = ProductDAO.getInstance();
        productReader = new ProductReader();
        productInitialization(productReader.readProductFromFile(Consts.PATH_TO_PRODUCT_LIST));

    }


    public void productInitialization(List<Product> products) {
        logger.info("Entered in product initialization with elements: "+products);
        for (Product product : products) {
            productDAO.insert(product);
        }
    }

    @Override
    public void insertProduct(Product product) {
        productDAO.insert(product);
    }

    @Override
    public Product getProduct(int id) {
        return productDAO.get(id);
    }

    @Override
    public List<Product> getAllProducts() {
        return productDAO.getAll();
    }

    @Override
    public void deleteProduct(int id) {
        productDAO.delete(id);
    }

    @Override
    public void deleteAllProducts() {
        productDAO.deleteAll();
    }

    @Override
    public void updateProduct(Product product) {
        productDAO.update(product);
    }

    public ProductReader getProductReader() {
        return productReader;
    }
}
