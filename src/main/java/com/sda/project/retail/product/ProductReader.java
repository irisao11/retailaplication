package com.sda.project.retail.product;

import com.sda.project.retail.client.Client;
import com.sda.project.retail.display.Display;
import com.sda.project.retail.utils.Consts;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ProductReader {
    final static Logger logger = Logger.getLogger(ProductReader.class);
    public List<Product> readProductFromFile(String path) {
        List<Product> products = new LinkedList<>();
        File inputFile = new File(path);
        try {
            FileReader fileReader = new FileReader(inputFile);
            BufferedReader reader = new BufferedReader(fileReader);

            String line = reader.readLine();
            while (line != null) {
                String[] elements = line.split(" ");

                Product product = new Product();

                product.setProductId(Integer.valueOf(elements[0]));
                product.setPrice(Integer.valueOf(elements[1]));
                product.setColour(elements[2]);
                product.setType(elements[3]);
                product.setBarCode(elements[4]);

                products.add(product);
                line = reader.readLine();
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            logger.info(products);
            logger.warn(products);
            logger.error(products);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return products;
    }

    public Product readProductFromKeyBoard(Scanner s){
        Product product = new Product();

        Display.displayMessage(Consts.INSERT_PRODUCT_ID);
        int productID = s.nextInt();
        product.setProductId(productID);

        Display.displayMessage(Consts.INSERT_PRODUCT_PRICE);
        int productPrice = s.nextInt();
        product.setPrice(productPrice);

        Display.displayMessage(Consts.INSERT_PRODUCT_COLOUR);
        String productColour = s.next();
        product.setColour(productColour);

        Display.displayMessage(Consts.INSERT_PRODUCT_TYPE);
        String productType = s.next();
        product.setType(productType);

        Display.displayMessage(Consts.INSERT_PRODUCT_BARCODE);
        String productBarCode = s.next();
        product.getBarCode();

    return product;
    }
}