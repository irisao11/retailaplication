package com.sda.project.retail.client;

import com.sda.project.retail.adress.Adress;

public class Client {
    private int clientId;
    private String name;
    private String CNP;
    private int age;
    private String phoneNumber;
    private Adress adress;

    public Client() {
    }

    public Client(int clientId, String name, String CNP, int age, String phoneNumber, Adress adress) {
        this.clientId = clientId;
        this.name = name;
        this.CNP = CNP;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.adress = adress;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Adress getAdress() {
        return adress;
    }

    public void setAdress(Adress adress) {
        this.adress = adress;
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientId=" + clientId +
                ", name='" + name + '\'' +
                ", CNP='" + CNP + '\'' +
                ", age=" + age +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", adress=" + adress +
                '}';
    }
}
