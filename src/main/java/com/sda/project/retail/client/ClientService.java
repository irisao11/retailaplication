package com.sda.project.retail.client;

import com.sda.project.retail.utils.Consts;
import com.sun.xml.internal.bind.v2.runtime.reflect.opt.Const;

import java.util.List;

//reprezinta al doilea layer;
public class ClientService implements ClientServiceInterface {
    private ClientDAO clientDAO;
    private ClientReader clientReader;

    public ClientService() {
        clientDAO = ClientDAO.getInstance();
        clientReader = new ClientReader();
        clientInitialization(clientReader.readClientFromFile(Consts.PATH_TO_CLIENT_LIST));
    }

    public void clientInitialization(List<Client> clients) {
        for (Client client : clients) {
            clientDAO.insert(client);
        }
    }

    public void insertClient(Client client) {
        clientDAO.insert(client);
    }

    @Override
    public Client getClient(int id) {
        return clientDAO.get(id);
    }

    @Override
    public List<Client> getAllClients() {
        return clientDAO.getAll();
    }

    @Override
    public void deleteClient(int id) {
        clientDAO.delete(id);
    }

    @Override
    public void deleteAllClients() {
        clientDAO.deleteAll();
    }

    @Override
    public void UpdateClient(Client client) {
        clientDAO.update(client);
    }

    public ClientReader getClientReader() {
        return clientReader;
    }
}
