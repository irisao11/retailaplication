package com.sda.project.retail.client;

import com.sda.project.retail.adress.Adress;
import com.sda.project.retail.display.Display;
import com.sda.project.retail.utils.Consts;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ClientReader {


    public List<Client> readClientFromFile(String path) {
        List<Client> clients = new LinkedList<>();
        File inputFile = new File(path);
        try {
            FileReader fileReader = new FileReader(inputFile);
            BufferedReader reader = new BufferedReader(fileReader);

            String line = reader.readLine();
            while (line != null) {
                String[] elements = line.split(" ");

                Adress adress = new Adress();
                Client client = new Client();

                client.setClientId(Integer.valueOf(elements[0]));
                client.setName(elements[1]);
                client.setCNP(elements[2]);
                client.setAge(Integer.valueOf(elements[3]));
                client.setPhoneNumber(elements[4]);

                adress.setNumber(Integer.valueOf(elements[5]));
                adress.setStreet(elements[6]);
                adress.setCountry(elements[7]);

                client.setAdress(adress);
                clients.add(client);

                line = reader.readLine();
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return clients;
    }

    public Client readClientFromKeyBoard(Scanner s) {
        Client client = new Client();
        Adress adress = new Adress();

        //se creaza pachet separat pentru toate displayurile cu o interfata in care se pun toate mesajele pe care
        //le vrem afisate. apelam clasa Display cu metoda displayMessage in care afisam
        //mesajul insert_client-id din interfata Consts


        Display.displayMessage(Consts.INSERT_CLIENT_ID);
        int clientID = s.nextInt();
        client.setClientId(clientID);

        Display.displayMessage((Consts.INSERT_CLIENT_NAME));
        String name = s.next();
        client.setName(name);

        Display.displayMessage(Consts.INSERT_CLIENT_AGE);
        int age = s.nextInt();
        client.setAge(age);

        Display.displayMessage(Consts.INSERT_CLIENT_CNP);
        String CNP = s.next();
        client.setCNP(CNP);

        Display.displayMessage(Consts.INSERT_CLIENT_PHONE_NUMBER);
        String phoneNumber = s.next();
        client.setPhoneNumber(phoneNumber);

        Display.displayMessage(Consts.INSERT_ADRESS_NUMBER);
        int number = s.nextInt();
        adress.setNumber(number);

        Display.displayMessage(Consts.INSERT_ADRESS_STREET);
        String street = s.next();
        adress.setStreet(street);

        Display.displayMessage(Consts.INSERT_ADRESS_COUNTRY);
        String country = s.next();
        adress.setCountry(country);

        client.setAdress(adress);


        return client;
    }
}
