package com.sda.project.retail.client;

import java.util.List;

public interface ClientServiceInterface {

    public void insertClient(Client client);

    public Client getClient(int id);

    public List<Client> getAllClients();

    public void deleteClient(int id);

    public void deleteAllClients();

    public void UpdateClient(Client client);

}
