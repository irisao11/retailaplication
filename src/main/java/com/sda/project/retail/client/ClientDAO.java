package com.sda.project.retail.client;

import com.sda.project.retail.utils.GenericCRUD;

import java.util.ArrayList;
import java.util.List;

//tip singleton - nu vreau sa modific lista mea de clienti
public class ClientDAO extends GenericCRUD<Client> {

    private List<Client> clients;
    //o variablia ClientDao pt ca avem singleton
    private static ClientDAO instance;


    //constructorul instantiaza o lista goala de clienti;este privat pt ca fol singleton
    private ClientDAO() {
        clients = new ArrayList<>();
    }

    public ClientDAO(List<Client> clients) {
        this.clients = clients;
    }

    //metoda de instantiere pt singleton
    public static ClientDAO getInstance() {
        if (instance == null) {
            instance = new ClientDAO();
        }
        return instance;
    }

    //suprascriu adauand elemente(clienti) in lista
    @Override
    public void insert(Client element) {
        this.clients.add(element);

    }

    //suprascriu parsand prin lista si verificand clientii dupa id
    @Override
    public Client get(int id) {
        for (Client client : clients) {
            if (id == client.getClientId()) ;
            return client;
        }
        return null;
    }

    @Override
    public List<Client> getAll() {
        return clients;
    }

    @Override
    public void delete(int id) {
        for (Client client : clients) {
            if (id == client.getClientId()) ;
            clients.remove(client);
            break;//optimizeaza, in sensul ca se va opri odata ce am sters clientul respectiv, nu mai itereaza prin lista mai departe
        }
    }

    //se poate si cu clients = new ArrayList<>;
    @Override
    public void deleteAll() {
        clients.clear();

    }

    //de refacut cu ifuri daca se modifica doar unul din campuri, de ex adresa
    @Override
    public void update(Client element) {
        for (Client client : clients) {
            if (element.getClientId() == client.getClientId()) {
                client.setAdress(element.getAdress());
                client.setCNP(element.getCNP());
                client.setAge(element.getAge());
                client.setName(element.getName());
                client.setPhoneNumber(element.getPhoneNumber());
            }
        }

    }
}
