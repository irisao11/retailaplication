package com.sda.project.retail.adress;

public class Adress {
    private int number;
    private String street;
    private String country;

    public Adress(){
    }
    public Adress(int number, String street, String country) {
        this.number = number;
        this.street = street;
        this.country = country;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Adress{" +
                "number=" + number +
                ", street='" + street + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
