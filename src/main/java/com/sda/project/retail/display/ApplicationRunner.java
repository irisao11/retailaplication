package com.sda.project.retail.display;

import com.sda.project.retail.logic.ApplicationLogic;

public class ApplicationRunner {

    public static void main(String[] args) {
        ApplicationLogic logic = new ApplicationLogic();
        logic.runLogic();
    }
}
