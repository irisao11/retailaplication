package com.sda.project.retail.logic;

import com.sda.project.retail.client.Client;
import com.sda.project.retail.client.ClientService;
import com.sda.project.retail.display.Display;
import com.sda.project.retail.product.Product;
import com.sda.project.retail.product.ProductService;
import com.sda.project.retail.utils.Consts;

import java.util.List;
import java.util.Scanner;

public class ApplicationLogic {
    private ClientService clientService;
    private ProductService productService;

    public ApplicationLogic() {
        clientService = new ClientService();
        productService = new ProductService();
    }

    public void runLogic() {
        Scanner s = new Scanner(System.in);
        boolean isRunning = true;
        Display.displayMessage(Consts.WELCOME_MESSAGE);

        while (isRunning) {
            Display.displayMessage(Consts.DISPLAY_ALL_CLIENTS);
            Display.displayMessage(Consts.DISPLAY_ALL_PRUDUCTS);
            Display.displayMessage(Consts.INSERT_NEW_CLIENT);
            Display.displayMessage(Consts.INSERT_NEW_PRODUCT);
            Display.displayMessage(Consts.EXIT_MESSAGE);
            Display.newLine();
            Display.displayMessage(Consts.INSERT_OPTION);

            int option = s.nextInt();
            switch (option) {
                case 1:
                    List<Client> clients = clientService.getAllClients();
                    Display.dislplayAllClients(clients);
                    break;
                case 2:
                    List<Product> products = productService.getAllProducts();
                    Display.displayAllProducts(products);
                    break;
                case 3:
                    Client client = clientService.getClientReader().readClientFromKeyBoard(s);
                    clientService.insertClient(client);
                    break;
                case 4:
                    Product product = productService.getProductReader().readProductFromKeyBoard(s);
                    productService.insertProduct(product);
                    break;
                case 5:
                    isRunning = false;
                    break;
                default:
                    throw new IllegalArgumentException("Please choose another option");
            }

        }
        s.close();
    }
}
