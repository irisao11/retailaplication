package com.sda.project.retail.utils;

public interface Consts {

    /*
    constante cai fisiere
     */
    public static final String PATH_TO_CLIENT_LIST = "C:\\Users\\irisa\\IdeaProjects\\RetailAplication\\src\\main\\resources\\client\\client.txt";
    public static final String PATH_TO_PRODUCT_LIST = "C:\\Users\\irisa\\IdeaProjects\\RetailAplication\\src\\main\\resources\\product\\product.txt";
    /*
    constante mesaje
     */

    public static final String INSERT_CLIENT_ID = "Please insert a client id";
    public static final String INSERT_CLIENT_NAME = "Please insert a client name";
    public static final String INSERT_CLIENT_CNP = "Please insert a client CNP";
    public static final String INSERT_CLIENT_AGE = "Please insert a client age";
    public static final String INSERT_CLIENT_PHONE_NUMBER = "Please insert a client phone number";

    /*
    constante adresa
     */

    public static final String INSERT_ADRESS_NUMBER = "Please insert an adress number";
    public static final String INSERT_ADRESS_STREET = "Please insert an adress street";
    public static final String INSERT_ADRESS_COUNTRY = "Please insert an adress country";

    /*
    constante produse
     */

    public static final String INSERT_PRODUCT_ID = "Please insert a product id";
    public static final String INSERT_PRODUCT_PRICE = "Please insert a product price";
    public static final String INSERT_PRODUCT_COLOUR = "Please insert a product colour";
    public static final String INSERT_PRODUCT_TYPE = "Please insert a product type";
    public static final String INSERT_PRODUCT_BARCODE = "Please insert a product barcode";

    /*
    general messages
     */
    public static final String WELCOME_MESSAGE = "Welcome to Emag!";
    public static final String DISPLAY_ALL_CLIENTS = "1. Display all clients!";
    public static final String DISPLAY_ALL_PRUDUCTS = "2. display all products!";
    public static final String INSERT_NEW_CLIENT = "3.Insert new client!";
    public static final String INSERT_NEW_PRODUCT = "4. Insert new product!";
    public static final String EXIT_MESSAGE = "5. Exit!";
    public static final String INSERT_OPTION = "Please insert an option:";
}
