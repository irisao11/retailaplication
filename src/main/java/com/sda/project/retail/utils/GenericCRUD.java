package com.sda.project.retail.utils;

import java.util.List;

//clasa imi foloseste pentru a apela aceste metode de catre toate celelalte clase(Product, Adress, Client
//genericele sunt tipuri ce pot fi fol in clase -  pot fi inlocuite cu ce vrei tu
//aici se fol ca o interfata, pt ca e o clasa abstracta
//in alte cazuri pot fol si metode
//cand folosim generice, trebuie sa le declaram in signatura clasei: Ex:T
public abstract class GenericCRUD<T> {
    public abstract void insert(T element);

    //in functie de id, atat in client cat si in product folosim un id
    public abstract T get(int id);

    public abstract List<T> getAll();

    public abstract void delete(int id);

    public abstract void deleteAll();

    public abstract void update(T element);


}
